export default {

  timeline: () => {
    let timeline = [];
    let x = -1000;

    while( x <= (new Date()).getFullYear()) {
      let decades = [];
      if (x !== 0) {
        let temp = x;
        while(temp <= x+99) {
          decades.push(temp)
          temp = temp + 11
        }
        timeline.push(decades)
        decades = []
      }
      x === 0 ? x = 1 : x = x + 100;
    }    
    return timeline;
  },

  flatten: (myarr) => {
    return myarr.reduce((acc, arr) => acc.concat(arr), [])
  },
  
  initTimeouts: (vueInst) => {
    for (let i = vueInst.timeoutSequence.length; i < vueInst.records.length; i++) {
      let timeoutID;
      ((i, id) => {
          timeoutID = setTimeout(() => {
          vueInst.record = vueInst.records[i];
          vueInst.currentRecordIDX = i;
          vueInst.timeline_range = rangeCheck(vueInst.timeline_flat, vueInst.record.yob, vueInst.record.yod)
        }, vueInst.delay*i);
        vueInst.timeoutSequence.push(timeoutID)
        
      })(i, timeoutID);
    };
  }
}

const rangeCheck = (timeline, yob, yod) => {
  const range = (a,b) => {
    let r = [a];
    for(let i=a;i<=b;i++){
         r.push(i);
    }
    return r;
  }
  
  let range_limit = range(yob, yod)
  
  let timeline_range = [];
  
  range_limit.forEach((time, idx) => {
    if(timeline.indexOf(time) > -1) {
      timeline_range.push(time)
    }
  });
  
  timeline_range = timeline_range.filter((each, idx) => timeline_range.indexOf(each) == idx).sort((a,b) => a - b);
  return timeline_range
}